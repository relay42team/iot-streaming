// @GENERATOR:play-routes-compiler
// @SOURCE:/home/albertr/Documents/Relay42-test/play-java-seed/conf/routes
// @DATE:Thu Dec 20 19:13:08 COT 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
