package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import repositories.SensorRepo;
import streams.processors.SensorMeasureProcessor;
import streams.processors.SensorMeasureStorage;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private final SensorMeasureProcessor sensorMeasureProcessor;
    private final SensorMeasureStorage sensorMeasureStorage;
    private final SensorRepo sensorRepo;

    @Inject
    public HomeController(SensorMeasureProcessor sensorMeasureProcessor, SensorRepo sensorRepo, SensorMeasureStorage sensorMeasureStorage) {
        this.sensorMeasureProcessor = sensorMeasureProcessor;
        this.sensorMeasureStorage = sensorMeasureStorage;
        this.sensorRepo = sensorRepo;
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(views.html.index.render());
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result getMeasures() {
        JsonNode json = request().body().asJson();
        UUID sensorId = UUID.fromString(json.get("sensorId").asText());
        LocalDateTime fromDate = this.getDateFromString(json.get("fromDate").asText());
        LocalDateTime toDate = this.getDateFromString(json.get("toDate").asText());
        int page = json.get("page").asInt();

        return ok(Json.toJson(this.sensorRepo.getSensorDataByIdAndDateArr(sensorId,
                fromDate,
                toDate,
                page)));
    }

    private LocalDateTime getDateFromString(String strDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(strDate, formatter);
    }

}
