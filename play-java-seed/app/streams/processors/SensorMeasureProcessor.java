package streams.processors;

import com.typesafe.config.Config;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import play.Logger;
import streams.pojos.SensorMeasure;
import streams.pojos.SensorMeasureResult;
import streams.serdes.FactorySerde;

import javax.inject.Inject;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

public class SensorMeasureProcessor {

    private final Config config;

    @Inject
    public SensorMeasureProcessor(Config config) {
        this.config = config;

        Logger.info("Broker: " + config.getString("kafkaBroker"));

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "sensor-measure-pipe");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, config.getString("kafkaBroker"));
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.UUID().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.Integer().getClass());

        final StreamsBuilder builder = new StreamsBuilder();
        KStream<UUID, SensorMeasure> source = builder.stream("temperature-topic", Consumed.with(Serdes.UUID(), FactorySerde.getSensorMeasureSerde()));

        // Counter
        KTable<UUID, Long> counter = source.groupBy((key, value) -> value.sensorId)
                .count(Materialized.as("counts-store"));
                //.count();
        //counter.toStream().foreach((key, value) -> Logger.info("SensorId: " + key + " counter: " + value));
        // Accumulator
        KTable<UUID, Integer> accumulator = source.map(((key, value) -> KeyValue.pair(value.sensorId, value.value)))
                .groupByKey()
                .aggregate(() -> 0, (key, newValue, aggValue) -> newValue + aggValue, Materialized.as("aggregator-store"));
                //.aggregate(() -> 0, (key, newValue, aggValue) -> newValue + aggValue);
        //accumulator.toStream().foreach((key, value) -> Logger.info("SensorId: " + key + " accumulator: " + value));

        // Average
        KTable<UUID, Long> average = counter
                .join(accumulator, (left, right) -> left != 0 ? right / left : 0);

        // Max
        KTable<UUID, Integer> max = source.map(((key, value) -> KeyValue.pair(value.sensorId, value.value)))
                .groupByKey()
                .reduce((aggValue, newValue) -> aggValue > newValue ? aggValue : newValue, Materialized.as("max-store"));
                //.reduce((aggValue, newValue) -> aggValue > newValue ? aggValue : newValue);

        // Min
        KTable<UUID, Integer> min = source.map(((key, value) -> KeyValue.pair(value.sensorId, value.value)))
                .groupByKey()
                .reduce((aggValue, newValue) -> aggValue < newValue ? aggValue : newValue, Materialized.as("min-store"));
                //.reduce((aggValue, newValue) -> aggValue < newValue ? aggValue : newValue);

        // Populate the result object
        max.join(min,
                ((left, right) -> {
                    SensorMeasureResult partialResult = new SensorMeasureResult();
                    partialResult.max = left;
                    partialResult.min = right;
                    return partialResult;
                }))
                .join(average,
                        ((left, right) -> {
                            left.average = right;
                            return left;
                        }))
                .toStream()
                .to("temperature-result-topic", Produced.with(Serdes.UUID(), FactorySerde.getSensorMeasureResultSerde()));


        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("Streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.wait();
        } catch (Throwable e) {
            Logger.error("Something bad happens!");
        }
    }
}
