package streams.processors;

import com.typesafe.config.Config;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.bson.types.ObjectId;
import play.Logger;
import repositories.SensorRepo;
import streams.pojos.SensorMeasureResult;
import streams.serdes.FactorySerde;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

public class SensorMeasureStorage {
    private final Config config;
    private SensorRepo sensorRepo;

    @Inject
    public SensorMeasureStorage(Config config, SensorRepo sensorRepo) {
        this.config = config;
        this.sensorRepo = sensorRepo;

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "sensor-storage-pipe");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, config.getString("kafkaBroker"));
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.UUID().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.Integer().getClass());

        final StreamsBuilder builder = new StreamsBuilder();

        KStream<UUID, SensorMeasureResult> resultKStream = builder.stream("temperature-result-topic", Consumed.with(Serdes.UUID(), FactorySerde.getSensorMeasureResultSerde()));
        resultKStream.groupByKey().reduce((oldValue, newValue) -> newValue).toStream().foreach(((key, value) -> {

            repositories.pojos.SensorMeasure sensorMeasure = new repositories.pojos.SensorMeasure();
            sensorMeasure.setAverage(value.average);
            sensorMeasure.setMax(value.max);
            sensorMeasure.setMin(value.min);
            sensorMeasure.setSensorId(key);
            sensorMeasure.setId(new ObjectId());
            sensorMeasure.setCreateDate(LocalDateTime.now());
            this.sensorRepo.saveSensorData(sensorMeasure);

            Logger.info("Stored => SensorId: " + sensorMeasure.getSensorId() + " Average: " + sensorMeasure.getAverage());
        }));


        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("Streams-shutdown-hook-2") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.wait();
        } catch (Throwable e) {
            Logger.error("Something bad happens! " + e.getMessage());
        }
    }
}
