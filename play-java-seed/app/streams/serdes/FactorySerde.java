package streams.serdes;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import streams.pojos.SensorMeasure;
import streams.pojos.SensorMeasureResult;
import streams.serializers.JsonPOJODeserializer;
import streams.serializers.JsonPOJOSerializer;

import java.util.HashMap;
import java.util.Map;

public class FactorySerde {

    static public  Serde<SensorMeasure> getSensorMeasureSerde() {
        // Sensor measure
        // Serializer
        Map<String, Object> serdeProps = new HashMap<>();
        final Serializer<SensorMeasure> sensorMeasureSerializer = new JsonPOJOSerializer<>();
        serdeProps.put("JsonPOJOClass", SensorMeasure.class);
        sensorMeasureSerializer.configure(serdeProps, false);
        // Deserializer
        final Deserializer<SensorMeasure> sensorMeasureDeserializer = new JsonPOJODeserializer<>();
        serdeProps.put("JsonPOJOClass", SensorMeasure.class);
        sensorMeasureDeserializer.configure(serdeProps, false);
        // Serde
        return Serdes.serdeFrom(sensorMeasureSerializer, sensorMeasureDeserializer);
    }

    static public  Serde<SensorMeasureResult> getSensorMeasureResultSerde() {
        // Sensor measure result
        // Serializer
        Map<String, Object> serdeProps = new HashMap<>();
        final Serializer<SensorMeasureResult> sensorMeasureResultSerializer = new JsonPOJOSerializer<>();
        serdeProps.put("JsonPOJOClass", SensorMeasureResult.class);
        sensorMeasureResultSerializer.configure(serdeProps, false);
        // Deserializer
        final Deserializer<SensorMeasureResult> sensorMeasureResultDeserializer = new JsonPOJODeserializer<>();
        serdeProps.put("JsonPOJOClass", SensorMeasureResult.class);
        sensorMeasureResultDeserializer.configure(serdeProps, false);
        // Serde
        return Serdes.serdeFrom(sensorMeasureResultSerializer, sensorMeasureResultDeserializer);
    }
}
