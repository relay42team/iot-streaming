package streams.pojos;

import java.util.UUID;

public class SensorMeasure {
    public UUID sensorId;
    public Integer value;
}
