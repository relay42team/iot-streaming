package streams.pojos;

public class  SensorMeasureResult {
    public String sensorId;
    public  Integer max;
    public Integer min;
    public Long average;
}
