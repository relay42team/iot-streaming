package repositories.pojos;

import com.mongodb.client.MongoDatabase;
import repositories.CreateMongoClient;

import javax.inject.Inject;

public class AccessMongoDataBase {

    private CreateMongoClient createMongoClient;
    private MongoDatabase mongoDatabase;

    @Inject
    public AccessMongoDataBase(CreateMongoClient createMongoClient) {
        this.createMongoClient = createMongoClient;
        this.mongoDatabase = this.createMongoClient.getMongoClient().getDatabase("sensors-data");
    }

    public MongoDatabase getMongoDatabase() {
        return mongoDatabase;
    }
}
