package repositories.pojos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class SensorMeasure {

    public static final String COLLECTION_NAME = "SensorMeasure";
    @JsonIgnore
    private ObjectId id;
    private UUID sensorId;
    @JsonIgnore
    private LocalDateTime createDate;
    private Integer max;
    private Integer min;
    private Long average;

    @JsonProperty("createDate")
    public String getStrCreateDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return this.createDate.format(formatter);
    }

    public ObjectId getId() {
        return id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }


    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Long getAverage() {
        return average;
    }

    public void setAverage(Long average) {
        this.average = average;
    }

    public UUID getSensorId() {
        return sensorId;
    }

    public void setSensorId(UUID sensorId) {
        this.sensorId = sensorId;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }
}
