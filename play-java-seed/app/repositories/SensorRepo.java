package repositories;

import com.google.inject.ImplementedBy;
import repositories.pojos.SensorMeasure;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ImplementedBy(MongoSensorRepo.class)
public interface SensorRepo {
    void saveSensorData(SensorMeasure sensorMeasure);
    Optional<SensorMeasure> getSensorDataByIdAndDate(UUID sensorId, LocalDateTime startDateTime, LocalDateTime endDateTime);
    List<SensorMeasure> getSensorDataByIdAndDateArr(UUID sensorId, LocalDateTime startDateTime, LocalDateTime endDateTime, int page);
}
