package repositories;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Sorts;
import repositories.pojos.AccessMongoDataBase;
import repositories.pojos.SensorMeasure;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.mongodb.client.model.Filters.*;

public class MongoSensorRepo implements SensorRepo {

    private final MongoDatabase mongoDatabase;
    private final MongoCollection<SensorMeasure> sensorMeasureMongoCollection;

    @Inject
    public MongoSensorRepo(AccessMongoDataBase mongoDataBase) {
        this.mongoDatabase = mongoDataBase.getMongoDatabase();
        this.sensorMeasureMongoCollection = this.mongoDatabase.getCollection(SensorMeasure.COLLECTION_NAME, SensorMeasure.class);
        this.sensorMeasureMongoCollection.createIndex(Indexes.ascending("createDate"));
        this.sensorMeasureMongoCollection.createIndex(Indexes.ascending("sensorId"));
    }

    @Override
    public void saveSensorData(SensorMeasure sensorMeasure) {
        this.sensorMeasureMongoCollection.insertOne(sensorMeasure);
    }

    @Override
    public Optional<SensorMeasure> getSensorDataByIdAndDate(UUID sensorId, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        SensorMeasure sensorMeasure = this.sensorMeasureMongoCollection
                .find(and(eq("sensorId", sensorId), gte("createDate", startDateTime), lte("createDate", endDateTime)))
                .first();
        return sensorMeasure != null ? Optional.of(sensorMeasure) : Optional.empty();
    }

    @Override
    public List<SensorMeasure> getSensorDataByIdAndDateArr(UUID sensorId, LocalDateTime startDateTime, LocalDateTime endDateTime, int page) {
        List<SensorMeasure> sensorMeasureArray = this.sensorMeasureMongoCollection
                .find(and(eq("sensorId", sensorId), gte("createDate", startDateTime), lte("createDate", endDateTime)))
                .sort(Sorts.descending("createDate"))
                .skip(page * 20)
                .limit(20)
                .into(new ArrayList<>());

        return sensorMeasureArray;
    }
}
