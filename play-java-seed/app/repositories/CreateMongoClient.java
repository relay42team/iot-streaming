package repositories;

import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.typesafe.config.Config;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;


@Singleton
public class CreateMongoClient {
    private MongoClient mongoClient;
    private final Config config;

    @Inject
    public CreateMongoClient(Config config) {
        this.config = config;

        Logger.info("MongoDb host: " + config.getString("mongodbHosts"));

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        MongoClientSettings settings = MongoClientSettings.builder()
                .applyToClusterSettings(builder -> builder.hosts(Arrays.asList(new ServerAddress(config.getString("mongodbHosts")))))
                .codecRegistry(pojoCodecRegistry)
                .build();
        this.mongoClient = MongoClients.create(settings);

    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }
}
