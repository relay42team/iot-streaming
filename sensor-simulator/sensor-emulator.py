#!/usr/bin/python
import json
import os
import time
from random import randrange, random
from kafka import KafkaProducer

# Kafka broker dir
broker = os.getenv('BROKER', 'localhost:9092')
print('Broker: ' + str(broker))
# Wait 30 seconds until kafka broker starts
time.sleep(30)

producer = KafkaProducer(bootstrap_servers=[broker], value_serializer=lambda v: json.dumps(v).encode('utf-8'))
# Create topic temperature-result-topic with base values
producer.send('temperature-result-topic', {'max': 0, 'min': 0, 'average': 0})

# SensorsIds, to simulate more sensors add more UUIDs to the array
sensorsIds = ['47d95341-b754-494c-b7f9-5c691ad7b680', 'd4e6565f-8aaa-4f3c-a464-5ef849c85960', 'da84008d-490d-4250-b524-1fd9521297be']
flag = True
while True:
    for i in sensorsIds:
        #value = randrange(0,150)
        value = random() * 500
        print('SensorId: ' + str(i) + ' value: ' + str(value))
        producer.send('temperature-topic', {'sensorId': i, 'value': value})
        if flag:
            # Wait 30 seconds until the Streaming processor app starts
            time.sleep(30)
            flag = False
    producer.flush()        
    time.sleep(1)
